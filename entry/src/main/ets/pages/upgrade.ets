/*
  * Copyright (c) 2022 Huawei Device Co., Ltd.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
    *
  * http://www.apache.org/licenses/LICENSE-2.0
    *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */

import dataRdb from '@ohos.data.relationalStore'
import { Migration } from '@ohos/dataorm'
import { Database } from '@ohos/dataorm'
import { DbUtils } from '@ohos/dataorm'
import { Student } from '../pages/test/Student'
import { Toolbar } from './toolbar'

var ctt: any;

@Entry
@Component
struct upgrade {
  @State arr: Array<Student> = new Array<Student>();
  private dbName: string = "testRgb.db";
  private tableName: string = "STUDENT";
  private version: number = 1
  @State importResult: string = "";

  createRgb() {
    const SQL_CREATE_TABLE = "CREATE TABLE IF NOT EXISTS STUDENT (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL)"
    dataRdb.getRdbStore(ctt, {
      name: this.dbName,
      securityLevel: dataRdb.SecurityLevel.S1,
      encrypt:false
    }, function (err, rdbStore) {
      if (err) {
        console.error('createRgb：err==' + err)
      } else {
        console.info('createRgb：getRdbStore')
        rdbStore.executeSql(SQL_CREATE_TABLE, null, function () {
          console.info('createRgb：create table done.')
        })
      }
    })
  }

  UpdateDB() {
    this.version = 2
    new Migration(this.dbName, this.tableName, 2).addColumn("AGE", "INTEGER").execute(ctt,false);
  }

  async aboutToAppear() {
    ctt = globalThis.contt
  }

  insertData() {
    dataRdb.getRdbStore(ctt, {
      name: this.dbName,
      securityLevel: dataRdb.SecurityLevel.S1,
      encrypt:false
    }, function (err, rdbStore) {
      if (err) {
        console.error('insertData i insertData err==' + err)
      } else {
        const valueBucket = {
          "NAME": "Lisa"
        }
        rdbStore.insert("STUDENT", valueBucket, function (err, ret) {
          console.info("insertData i insert first done: " + ret)
        })
      }
    })
  }

  NewinsertData() {
    dataRdb.getRdbStore(ctt, {
      name: this.dbName,
      securityLevel: dataRdb.SecurityLevel.S1,
      encrypt:false
    }, function (err, rdbStore) {
      if (err) {
        console.error('insertData test：err==' + err)
      } else {
        const valueBucket = {
          "NAME": "Lisa",
          "AGE": 5
        }
        rdbStore.insert("STUDENT", valueBucket, function (err, ret) {
          console.info("insert first done: " + ret)
        })
      }
    })
  }

  QueryDB1() {
    let that = this
    dataRdb.getRdbStore(ctt, {
      name: this.dbName,
      securityLevel: dataRdb.SecurityLevel.S1,
      encrypt:false
    }, function (err, rdbStore) {
      if (err) {
        console.error('test：err==' + err)
      } else {
        console.info('test：getRdbStore')
        let predicates = new dataRdb.RdbPredicates("STUDENT")
        predicates.equalTo("NAME", "Lisa")
        rdbStore.query(predicates, ["ID", "NAME"], function (err, resultSet) {
          let datas = new Array<Student>();
          while (resultSet.goToNextRow()) {
            let id = resultSet.getLong(resultSet.getColumnIndex("ID"))
            let name = resultSet.getString(resultSet.getColumnIndex("NAME"))
            let student = new Student();
            student.setId(id);
            student.setName(name);
            datas.push(student)
            console.info("id===>" + id + "name====>" + name)
          }
          that.arr = datas;
        })
      }
    })
  }

  BeiQueryDB1() {
    let dbName = 'testRgb-' + this.version + '.db'
    var that = this;
    dataRdb.getRdbStore(ctt, {
      name: this.dbName,
      securityLevel: dataRdb.SecurityLevel.S1,
      encrypt:false
    }, function (err, rdbStore) {
      if (err) {
        console.error('test i Backup  to restore failed ,getRdbStore err: ' + err.message)
        return;
      }
      dataRdb.deleteRdbStore(ctt, that.dbName, function (err) {
        if (err) {
          console.error("test i Delete RdbStore failed, err: " + err)
          return
        }
        rdbStore.restore(dbName, function (err) {
          if (err) {
            console.error('Restore failed, err: ' + err)
            return
          }
          console.info('Restore success.')
        })
      })
    })
  }

  QueryDB() {
    var that = this;
    dataRdb.getRdbStore(ctt, {
      name: this.dbName,
      securityLevel: dataRdb.SecurityLevel.S1,
      encrypt:false
    }, function (err, rdbStore) {
      if (err) {
        console.error('test:err==' + err)
      } else {
        console.info('test:getRdbStore')
        let predicates = new dataRdb.RdbPredicates("STUDENT")
        predicates.equalTo("NAME", "Lisa")
        rdbStore.query(predicates, ["ID", "NAME", "AGE"], function (err, resultSet) {
          let datas = new Array<Student>();
          while (resultSet.goToNextRow()) {
            let id = resultSet.getLong(resultSet.getColumnIndex("ID"))
            let name = resultSet.getString(resultSet.getColumnIndex("NAME"))
            let age = resultSet.getLong(resultSet.getColumnIndex("AGE"))
            let student = new Student();
            student.setId(id);
            student.setName(name);
            student.setAge(age);
            datas.push(student)
            console.info("id===>" + id + "name====>" + name + "age===>>" + age)
          }
          that.arr = datas;
        })
      }
    })
  }

  BeiDb() {
    let dbName = 'testRgb-' + this.version + '.db'
    Migration.backupDB(this.dbName, dbName, this.version, ctt,false)
  }

  async BeiDb22() {
    let database: Database = globalThis.daoSession.getDatabase()
    let n = await DbUtils.executeSqlScript(globalThis.contt.resourceManager, database, "minimal-entity.sql");
    let that = this;
    let per = database.getRawDatabase().querySql("SELECT * from MINIMAL_ENTITY");
    per.then((resultSet) => {
      console.log("BeiDb22 ResultSet row count: " + resultSet.rowCount)
      let str = '';
      if (resultSet && resultSet.goToFirstRow()) {
        let colCount: number = resultSet.columnCount;
        do {
          for (let i = 0;i < colCount; i++) {
            let column = resultSet.getColumnName(i);
            let index = resultSet.getColumnIndex(column);
            let res = resultSet.getString(index);
            str += '{' + column + ':' + res + '},';
          }

        } while (resultSet.goToNextRow());
        that.importResult = str;
      }
    })
  }

  BeiQueryDB() {
    var that = this;
    try {
      dataRdb.getRdbStore(ctt, {
        name: 'testRgb-2.db',
        securityLevel: dataRdb.SecurityLevel.S1,
        encrypt:false
      }, function (err, rdbStore) {
        if (!err) {
          let predicates = new dataRdb.RdbPredicates("STUDENT")
          let datas = new Array<Student>();
          rdbStore.query(predicates, ["ID", "NAME", "AGE"], function (err, resultSet) {
            while (resultSet.goToNextRow()) {
              let id = resultSet.getLong(resultSet.getColumnIndex("ID"))
              let name = resultSet.getString(resultSet.getColumnIndex("NAME"))
              let age = resultSet.getLong(resultSet.getColumnIndex("AGE"))
              let student = new Student();
              student.setId(id);
              student.setName(name);
              student.setAge(age);
              datas.push(student)
            }
            that.arr = datas;
          })
        }
      })
    } catch (e) {
      console.error("err_msg:" + e.message + "--err:" + e.stack)
    }
  }

  build() {
    Flex({ direction: FlexDirection.Column }) {
      Toolbar({ title: 'UPGRADE', isBack: true })
      Flex({ direction: FlexDirection.Row, wrap: FlexWrap.Wrap }) {
        Button('创建数据库')
          .fontSize(20)
          .margin({ left: 18, top: 5 })
          .onClick(() => {
            this.createRgb()
          })
        Button('添加数据')
          .fontSize(20)
          .height(45)
          .margin({ left: 18, top: 5 })
          .onClick(this.insertData.bind(this))
        Button('备份数据库')
          .fontSize(20)
          .height(45)
          .margin({ left: 18, top: 5 })
          .fontColor(0xFFFFFF)
          .onClick(this.BeiDb.bind(this))
          .backgroundColor(Color.Blue)
        Button('从备份中恢复')
          .fontSize(20)
          .height(45)
          .margin({ left: 18, top: 5 })
          .onClick(this.BeiQueryDB1.bind(this))
        Button('查询数据')
          .fontSize(20)
          .height(45)
          .margin({ left: 18, top: 5 })
          .onClick(this.QueryDB1.bind(this))
        Button('数据库升级')
          .fontSize(20)
          .height(45)
          .margin({ left: 18, top: 5 })
          .onClick(this.UpdateDB.bind(this))
          .backgroundColor(Color.Red)
        Button('升级后添加数据')
          .fontSize(20)
          .height(45)
          .margin({ left: 18, top: 5 })
          .onClick(this.NewinsertData.bind(this))
          .backgroundColor(Color.Red)

        Button('升级后查询数据')
          .fontSize(20)
          .height(45)
          .margin({ left: 18, top: 5 })
          .onClick(this.QueryDB.bind(this))
          .backgroundColor(Color.Red)
        Button('升级后查询备份数据库')
          .fontSize(20)
          .height(45)
          .margin({ left: 18, top: 5 })
          .onClick(this.BeiQueryDB.bind(this))
          .backgroundColor(Color.Red)
        Button('数据导入')
          .fontSize(20)
          .height(60)
          .margin({ left: 18, top: 5 })
          .onClick(this.BeiDb22.bind(this))
          .backgroundColor(Color.Blue)

      }.margin({ top: 12 })

      Flex({}) {
        List({ space: 20, initialIndex: 0 }) {
          ForEach(this.arr, (item) => {
            ListItem() {
              Flex({ direction: FlexDirection.Column }) {
                Text('' + JSON.stringify(item))
                  .fontSize(13)
              }.width('100%')
            }
          }, item => item.id)
        }

      }.margin({ top: 12 })

      Text('数据导入结果：' + this.importResult)
        .fontSize(20)
        .margin({ left: 18, top: 5 })

    }
    .width('100%')
    .height('100%')
  }
}